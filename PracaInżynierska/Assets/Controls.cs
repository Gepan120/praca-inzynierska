using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{
    [SerializeField] private AudioClip click;
    public void ShowControls()
    {
        SoundManager.instance.PlaySound(click);
        gameObject.SetActive(true);
    }
    public void Close()
    {
        SoundManager.instance.PlaySound(click);
        gameObject.SetActive(false);
    }
}
