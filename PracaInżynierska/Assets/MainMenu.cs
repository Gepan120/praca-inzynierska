using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private AudioClip click;
    public void ExitButton()
    {
        SoundManager.instance.PlaySound(click);
        Application.Quit();
    }
    public void StartGame()
    {
        SoundManager.instance.PlaySound(click);
        SceneManager.LoadScene("Game");
        StateNameController.points = 0;
    }
}
