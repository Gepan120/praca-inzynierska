using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reword : MonoBehaviour
{
    private Roomtemplate templates;
    private int rand;
    private Health myHealth;
    private float healthlevel;
    private bool spawned = false;

    void Start()
    {
        myHealth = GetComponent<Health>();
        templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<Roomtemplate>();

    }
    void Update()
    {
        healthlevel = myHealth.currenthealth;
        if (healthlevel <= 0)
        {
            if (spawned == false)
            {
                rand = Random.Range(0, templates.food.Length * 2);
                if (rand > templates.food.Length)
                {
                    Debug.Log("Brak nagrody");
                }
                else
                {
                    Instantiate(templates.food[rand], transform.position, templates.food[rand].transform.rotation);
                }
                spawned = true;
            }
        }
    }


}
