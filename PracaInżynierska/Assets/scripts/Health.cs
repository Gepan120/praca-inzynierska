using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float starthealth;
    [SerializeField] bool m_noBlood = false;
    [SerializeField] private int points;
    public float currenthealth { get; private set; }
    private Animator anim;

    private bool dead = false;
    public bool vulnerability { get; private set; }
    [SerializeField] private Behaviour[] components;
    [SerializeField] private AudioClip hurtsfx;
    [SerializeField] private AudioClip deathsfx;
    [SerializeField] private AudioClip block;


    private void Awake()
    {
        if(gameObject.tag == "Player")
        {
            currenthealth = StateNameController.herohealth;
        }
        else { 
            currenthealth = starthealth;
        }
        anim = GetComponent<Animator>();
        vulnerability = true;

    }
    private void Update()
    {

    }
    public void TakeDemage(float _damage)
    {
        if (gameObject.tag == "Player")
        {
            if(vulnerability == true)
            {
             currenthealth = Mathf.Clamp(currenthealth - _damage, 0, starthealth);
                StateNameController.herohealth = currenthealth;
                anima(currenthealth);
            }
            else
            {
                currenthealth = currenthealth;
                SoundManager.instance.PlaySound(block);
            }
        }
        else
        {
            currenthealth = Mathf.Clamp(currenthealth - _damage, 0, starthealth);
            anima(currenthealth);
        }

        
    }
    public void RegenHealth(float _value)
    {
        currenthealth = Mathf.Clamp(currenthealth + _value, 0, starthealth);
    }
    
    private void Deactivate()
    {
        gameObject.SetActive(false);

        if (gameObject.tag == "enemy")
        {
            StateNameController.points += points;
        }
    }
    public void vulnerable()
    {
        vulnerability = true;
    }

    public void invulnerable()
    {
        vulnerability = false;
    }
    private void anima(float health)
    {
        if (health > 0)
        {
            anim.SetTrigger("Hurt");
            SoundManager.instance.PlaySound(hurtsfx);
        }

        else
        {
            if (!dead)
            {
                if (gameObject.tag == "Player")
                {
                    anim.SetBool("noBlood", m_noBlood);
                    anim.SetTrigger("Death");
                    foreach (Behaviour component in components)
                    {
                        component.enabled = false;
                    }
                    dead = true;
                    SoundManager.instance.PlaySound(deathsfx);
                }
                else
                {
                    anim.SetTrigger("Death");
                    dead = true;
                    SoundManager.instance.PlaySound(deathsfx);
                    foreach (Behaviour component in components)
                    {
                        component.enabled = false;
                    }
                }


            }
        }
    }
}
