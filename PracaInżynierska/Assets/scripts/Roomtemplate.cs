using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roomtemplate : MonoBehaviour
{
    public GameObject[] HL;
    public GameObject[] LU;
    public GameObject[] RU;
    public GameObject[] HR;
    public GameObject[] LR;
    public GameObject[] RD;
    public GameObject[] LD;
    public GameObject[] LL;

    public GameObject closedroom;
    public GameObject[] start;

    public List<GameObject> rooms;
    public GameObject[] objects;
    public GameObject doors;
    public float waitTime;
    private bool spawnedDoor;
    private int rand;
    public GameObject[] food;

    void Update()
    {
        if(waitTime <=0 && spawnedDoor == false)
        {
            for (int i=0; i< rooms.Count; i++)
            {
                if (i < rooms.Count - 1)
                {
                    rand = Random.Range(0, objects.Length);
                    Instantiate(objects[rand], rooms[i].transform.position, Quaternion.identity);
                }
                else if (i == rooms.Count - 1)
                {
                    Instantiate(doors, rooms[i].transform.position, Quaternion.identity);
                    spawnedDoor = true;
                }
            }
        }
        else
        {
            waitTime -= Time.deltaTime;
        }
    }
}
