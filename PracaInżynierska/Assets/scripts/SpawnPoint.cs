using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public int openingDirection;
    /* 1 - need Highleft
     * 2 - need left up
     * 3 - need right up
     * 4 - need highright
     * 5 - need low right
     * 6 -need right down
     * 7 - need left down
     * 8 - need low left */
    private Roomtemplate templates;
    private int rand;
    public bool spawned;
    public float waitTime = 4f;

    void Start()
    {
        Destroy(gameObject, waitTime);
        templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<Roomtemplate>();
        Invoke("Spawn", 0.1f);
    }

    

    void Spawn()
    {
        if (spawned == false)
        {
            if (openingDirection == 1)
            {
                //hl
                rand = Random.Range(0, templates.HL.Length);
                Instantiate(templates.HL[rand], transform.position, templates.HL[rand].transform.rotation);
            }
            else if (openingDirection == 2)
            {
                //lu
                rand = Random.Range(0, templates.LU.Length);
                Instantiate(templates.LU[rand], transform.position, templates.LU[rand].transform.rotation);
            }
            else if (openingDirection == 3)
            {
                //ru
                rand = Random.Range(0, templates.RU.Length);
                Instantiate(templates.RU[rand], transform.position, templates.RU[rand].transform.rotation);
            }
            else if (openingDirection == 4)
            {
                //hr
                rand = Random.Range(0, templates.HR.Length);
                Instantiate(templates.HR[rand], transform.position, templates.HR[rand].transform.rotation);
            }
            else if (openingDirection == 5)
            {
                //lr
                rand = Random.Range(0, templates.LR.Length);
                Instantiate(templates.LR[rand], transform.position, templates.LR[rand].transform.rotation);
            }
            else if (openingDirection == 6)
            {
                //rd
                rand = Random.Range(0, templates.RD.Length);
                Instantiate(templates.RD[rand], transform.position, templates.RD[rand].transform.rotation);
            }
            else if (openingDirection == 7)
            {
                //ld
                rand = Random.Range(0, templates.LD.Length);
                Instantiate(templates.LD[rand], transform.position, templates.LD[rand].transform.rotation);
            }
            else if (openingDirection == 8)
            {
                //ll
                rand = Random.Range(0, templates.LL.Length);
                Instantiate(templates.LL[rand], transform.position, templates.LL[rand].transform.rotation);
            }
            else if (openingDirection == 9)
            {
                //Start
                rand = Random.Range(0, templates.start.Length);
                Instantiate(templates.start[rand], transform.position, templates.start[rand].transform.rotation);
            }
            spawned = true;
        }
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Spawnpoint"))
        {
            if(other.GetComponent<SpawnPoint>().spawned == false && spawned == false)
            {
                Instantiate(templates.closedroom, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
            spawned = true;
        }
    }

}
