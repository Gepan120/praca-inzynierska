using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] private AudioClip click;
    [SerializeField] private KeyCode esc;
    [SerializeField] private GameObject menu;
    private bool activated = false;
    public void Update()
    {
        if (Input.GetKeyDown(esc))
            {
            if (activated == false)
            {
                menu.SetActive(true);
                activated = true;
                Debug.Log("Aktywne");
            }
            else
            {
                menu.SetActive(false);
                activated = false;
                Debug.Log("Nieaktywne");
            }
        }
    }
    public void Restart()
    {
        SoundManager.instance.PlaySound(click);
        SceneManager.LoadScene("Game");
        StateNameController.points = 0;
        StateNameController.herohealth = 10;
    }
    public void exit()
    {
        SoundManager.instance.PlaySound(click);
        SceneManager.LoadScene("MainMenu");
        StateNameController.points = 0;
        StateNameController.herohealth = 10;
    }
}
