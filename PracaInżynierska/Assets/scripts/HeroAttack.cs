using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAttack : MonoBehaviour
{
    [SerializeField] private float range;
    [SerializeField] private float colliderdistance;
    [SerializeField] private int damage;
    [SerializeField] private BoxCollider2D boxCollider;
    [SerializeField] private LayerMask targetLayer;
    [SerializeField] private HeroKnight hk;
    [SerializeField] private AudioClip attacksfx;

    private float m_timeSinceAttack = 0.0f;
    private Animator anim;
    private Health targetHealth;
    private int m_currentAttack = 0;
    private bool m_rolling;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
        m_rolling = hk.m_rolling;

        // Increase timer that controls attack combo
        m_timeSinceAttack += Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && m_timeSinceAttack > 0.25f && !m_rolling)
        {

            m_currentAttack++;

            // Loop back to one after third attack
            if (m_currentAttack > 3)
            m_currentAttack = 1;
            

            // Reset Attack combo if time since last attack is too large
            if (m_timeSinceAttack > 1.0f)
            m_currentAttack = 1;
                
            

            // Call one of three attack animations "Attack1", "Attack2", "Attack3"
            anim.SetTrigger("Attack" + m_currentAttack);


            // Reset timer
            m_timeSinceAttack = 0.0f;
        }
    }
    private bool TargetInRange()
    {
        RaycastHit2D hit = Physics2D.BoxCast(boxCollider.bounds.center + transform.right * range * transform.localScale.x * colliderdistance, new Vector3(boxCollider.bounds.size.x * range, boxCollider.bounds.size.y, boxCollider.bounds.size.z), 0, Vector2.left, 0, targetLayer);


        if (hit.collider != null)
        {
            targetHealth = hit.transform.GetComponent<Health>();
        }
        return hit.collider != null;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boxCollider.bounds.center + transform.right * range * transform.localScale.x * colliderdistance, new Vector3(boxCollider.bounds.size.x * range, boxCollider.bounds.size.y, boxCollider.bounds.size.z));
    }
    private void DamageTarget()
    {
        if (TargetInRange())
        {
            targetHealth.TakeDemage(damage);

        }
    }
    private void attacksound()
    {
        SoundManager.instance.PlaySound(attacksfx);
    }
} 

