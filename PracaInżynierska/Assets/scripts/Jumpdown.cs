using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumpdown : MonoBehaviour
{


    void Update()
    {
        if (Input.GetKeyDown("s"))
        {
            if (gameObject.tag == "platform")
            {
                GetComponent<Collider2D>().enabled = false;
                Invoke("turnon", 0.5f);
            }

        }
    }

    public void turnon()
    {
        GetComponent<Collider2D>().enabled = true;
    }
}
