using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    [SerializeField] private float hregen;
    [SerializeField] private int points;
    [SerializeField] private AudioClip consume;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            SoundManager.instance.PlaySound(consume);
            other.gameObject.GetComponent<Health>().RegenHealth(hregen);
            gameObject.SetActive(false);
            StateNameController.points += points;
        }
    }
}
