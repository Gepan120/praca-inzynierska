using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public Text punktacja;
    [SerializeField] private AudioClip click;
    public void Setup()
    {
                gameObject.SetActive(true);
        punktacja.text = "Tw�j wynik to: " + StateNameController.points;
    }
    public void Restart()
    {
        SoundManager.instance.PlaySound(click);
        SceneManager.LoadScene("Game");
        StateNameController.points = 0;
        StateNameController.herohealth = 10;
    }
    public void exit()
    {
        SoundManager.instance.PlaySound(click);
        SceneManager.LoadScene("MainMenu");
        StateNameController.points = 0;
        StateNameController.herohealth = 10;
    }
}
